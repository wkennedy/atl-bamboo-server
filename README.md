defaults:
main.yml

handlers:
main.yml

tasks:
main.yml

templates:
bamboo-init.properties.j2
bamboo.service.j2
context.xml.j2
